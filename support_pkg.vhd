
-- Libraries to use in this file
library work;
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;

package support_pkg is
    -- Helper function by Prof. Dr. Schaefers (HAW-Hamburg)
    function numberOfBits(constant value : in positive) return positive;
end package support_pkg;

package body support_pkg is
    function numberOfBits(constant value : in positive) return positive is
        variable bitCnt_v : natural;
        variable cRest_v  : natural;
    begin
        cRest_v  := value;
        bitCnt_v := 0;
        while cRest_v > 0 loop
            cRest_v  := cRest_v / 2;
            bitCnt_v := bitCnt_v + 1;
        end loop;
        return bitCnt_v;
    end function numberOfBits;
end package body support_pkg;
