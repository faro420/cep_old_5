
library work;
  use work.all;
  use work.support_pkg.all;
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.std_logic_arith.all;

entity cea5 is
  generic (
      bitWidth : positive := 16
  );
  port (
    data : inout std_logic_vector(bitWidth-1 downto 0); -- Tristate bus
    --
    req  : in  std_logic; -- Request
    rnw  : in  std_logic; -- Read not write
    ack  : out std_logic; -- Acknowledge
    --
    nresSW : in std_logic;
    nres   : in std_logic;
    clk    : in std_logic;
    -- debug
    state_debug : out std_logic_vector(2 downto 0);
    ready_debug : out std_logic;
    request_debug : out std_logic;
    ack_debug : out std_logic;
    rnw_debug : out std_logic
  );
  constant msbPos : natural := bitWidth-1;
end entity;

architecture arch of cea5 is
  signal dati_m2c_s : std_logic_vector(msbPos downto 0); -- von uC zu Controller
  signal dato_c2m_s : std_logic_vector(msbPos downto 0); -- von Controller zu uC
  signal dati_s2c_s : std_logic_vector(msbPos downto 0); -- Result der Berechnung
  signal dato_c2s_s : std_logic_vector(msbPos downto 0); -- Input für Berechnung
  --
  signal rdy_s2c_s : std_logic; -- Slave => Controller (FPGA is ready for next calculation)
  signal req_c2s_s : std_logic; -- Controller => Slave (Request for calculation)
  signal req_m2c_s : std_logic;
  signal rnw_m2c_s : std_logic;
  signal ack_c2m_s : std_logic; -- Ack an uC
  signal oe_s      : std_logic; -- output enable (for tristate multiplexer)
  signal nres_s    : std_logic;
  signal clk_s     : std_logic;
  --
  -- DEBUG
  signal state_debug_s : std_logic_vector(2 downto 0);
  signal ready_debug_s : std_logic;
  signal request_debug_s : std_logic;
  --
  component gSync is
    generic (
      numberOfSyncFF : positive := 3  -- Number Of sync FFs (of synchronizer)
    );--]generic
    port (
      synco   : out  std_logic;       -- Out (synchroniced)
      synci   : in   std_logic;       -- In  (to be synchronized)
      clk     : in   std_logic;       -- CLocK
      sReset  : in   std_logic        -- Synchronous RESET (low aktiv)
    );--]port
  end component gSync;
  for all : gSync use entity work.gSync( rtl );
  --
  component gcd_controller is
    port (
      dati_m2c : in  std_logic_vector(15 downto 0); -- Zu berechnende Daten (uC zu Controller)
      dato_c2m : out std_logic_vector(15 downto 0); -- berechnete Daten (Controller zu uC)
      dati_s2c : in  std_logic_vector(15 downto 0); -- berechnete Daten (FPGA zu Controller)
      dato_c2s : out std_logic_vector(15 downto 0); -- Zu berechnende Daten (Controller zu FPGA)
      --
      req_m2c  : in std_logic; -- Request vom uC an Controller
      rnw_m2c  : in std_logic; -- Lesen (high) /Schreiben (low) von uC an Controller
      --
      req_c2s  : out std_logic; -- Request an Slave
      rdy_s2c  : in  std_logic; -- Slave is ready für neue berechnung bzw. ist fertig
      --
      ack_c2m  : out std_logic; -- Ack controller zu uC
      --
      oe       : out std_logic; -- output enable (für TriState)
      --
      nres     : in std_logic;
      clk      : in std_logic;
      state_debug : out std_logic_vector(2 downto 0)
    );
  end component gcd_controller;
  for all : gcd_controller use entity work.gcd_controller( rtl );
  --
  component sqrt_root is
    generic (
      bitWidth : natural := 16 -- Q-Format
    );
    port(
      fx   : out std_logic_vector( 15 downto 0 );
      rdy  : out std_logic;
      --
      x    : in  std_logic_vector( 15 downto 0 );
      req  : in  std_logic;
      clk  : in  std_logic;
      nRes : in  std_logic
    );
  end component sqrt_root;
  for all : sqrt_root use entity work.sqrt_root(arch);
begin
  -- synchronize asynchronous REQuest signal from master/µC
  syncMReq : gSync
    generic map (
      numberOfSyncFF  =>  2
    )--]generic map
    port map (
      synco  => req_m2c_s,
      synci  => req,
      clk    => clk_s,
      sReset => nres_s
    )--]port map
  ;--]syncMReq
  syncMRnw : gSync
    generic map (
      numberOfSyncFF  =>  2
    )--]generic map
    port map (
      synco  => rnw_m2c_s,
      synci  => rnw,
      clk    => clk_s,
      sReset => nres_s
    )--]port map
  ;--]syncMRnw
  syncCAck : gSync
    generic map (
      numberOfSyncFF  =>  2
    )--]generic map
    port map (
      synco  => ack,
      synci  => ack_c2m_s,
      clk    => clk_s,
      sReset => nres_s
    )--]port map
  ;--]syncMRnw
  -- gcd_controller instance
  gcd_controller_i : gcd_controller
    port map(
      dati_m2c => dati_m2c_s,
      dato_c2m => dato_c2m_s,
      dati_s2c => dati_s2c_s,
      dato_c2s => dato_c2s_s,
      req_m2c  => req_m2c_s,
      rnw_m2c  => rnw_m2c_s,
      req_c2s  => req_c2s_s,
      rdy_s2c  => rdy_s2c_s,
      ack_c2m  => ack_c2m_s,
      oe => oe_s,
      nres => nres_s,
      clk => clk_s,
      state_debug => state_debug_s
    )
  ;
  -- Square Root computer...
  sqrt_root_i : sqrt_root
    port map(
      fx  => dati_s2c_s,
      rdy => rdy_s2c_s,
      x   => dato_c2s_s,
      req => req_c2s_s,
      clk => clk_s,
      nRes => nres_s
    )
  ;
  --
  -- control drivers of bidirectional data bus
  tristate:
  process ( oe_s, dato_c2m_s ) is
  begin
    if oe_s = '1' then
      data <= dato_c2m_s;--dati_m2c_s;
    else
      data <= (others => 'Z');
    end if;
  end process tristate;
  --
  dati_m2c_s <= data;
  --
  nres_s <= nres and nresSW;
  clk_s  <= clk;
  -- DEBUG
  state_debug <= state_debug_s;
  ready_debug <= rdy_s2c_s;
  request_debug <= req_c2s_s;
  ack_debug <= ack_c2m_s;
  rnw_debug <= rnw;
end architecture arch;
