
library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity gcd_controller is
    generic (
        bitWidth : positive := 16 -- bit width - determines argument/operand size
    );--]generic
    port (
      dati_m2c : in  std_logic_vector(bitWidth-1 downto 0); -- Zu berechnende Daten (uC zu Controller)
      dato_c2m : out std_logic_vector(bitWidth-1 downto 0); -- berechnete Daten (Controller zu uC)
      dati_s2c : in  std_logic_vector(bitWidth-1 downto 0); -- berechnete Daten (FPGA zu Controller)
      dato_c2s : out std_logic_vector(bitWidth-1 downto 0); -- Zu berechnende Daten (Controller zu FPGA)
      --
      req_m2c  : in std_logic; -- Request vom uC an Controller
      rnw_m2c  : in std_logic; -- Lesen (high) /Schreiben (low) von uC an Controller
      --
      req_c2s  : out std_logic; -- Request an Slave
      rdy_s2c  : in  std_logic; -- Slave is ready für neue berechnung bzw. ist fertig
      --
      ack_c2m  : out std_logic; -- Ack controller zu uC
      --
      oe       : out std_logic; -- output enable (für TriState)
      --
      nres     : in std_logic;
      clk      : in std_logic;
      state_debug : out std_logic_vector(2 downto 0)
    );
    constant msbPos : natural := bitWidth-1;
end entity gcd_controller;

architecture rtl of gcd_controller is
  -- Internal Data Signals
  signal ack_c2m_cs  : std_logic := '0';
  signal ack_c2m_ns  : std_logic;
  --
  signal dati_m2c_cs : std_logic_vector(msbPos downto 0) := (others => '0');
  --
  signal dato_c2m_cs : std_logic_vector(msbPos downto 0) := (others => '0');
  signal dato_c2m_ns : std_logic_vector(msbPos downto 0);
  --
  signal dati_s2c_cs : std_logic_vector(msbPos downto 0) := (others => '0');
  --
  signal dato_c2s_cs : std_logic_vector(msbPos downto 0) := (others => '0');
  signal dato_c2s_ns : std_logic_vector(msbPos downto 0);
  --
  signal req_m2c_cs  : std_logic := '0';
  signal rnw_m2c_cs  : std_logic := '0';
  signal rdy_s2c_cs  : std_logic := '0';
  --
  signal req_c2s_cs  : std_logic := '0';
  signal req_c2s_ns  : std_logic;
  -- output enable
  signal oe_cs       : std_logic := '0';
  signal oe_ns       : std_logic;
  -- State Signals
  signal state_cs : std_logic_vector(2 downto 0) := (others => '0');
  signal state_ns : std_logic_vector(2 downto 0);
begin
  cobilo:
  process (state_cs, rnw_m2c_cs, req_m2c_cs, rdy_s2c_cs,
           ack_c2m_cs, dati_m2c_cs, dato_c2m_cs, dati_s2c_cs,
           dato_c2s_cs, req_c2s_cs, oe_cs) is
    variable state_v   : std_logic_vector(2 downto 0);
    variable rnw_v     : std_logic;
    variable req_v     : std_logic;
    variable rdy_s2c_v : std_logic;
    variable ack_c2m_v : std_logic;
    variable oe_v      : std_logic;
    -- variables datas
    variable dati_m2c_v : std_logic_vector(msbPos downto 0);
    variable dato_c2m_v : std_logic_vector(msbPos downto 0);
    variable dati_s2c_v : std_logic_vector(msbPos downto 0);
    variable dato_c2s_v : std_logic_vector(msbPos downto 0);
    --
    variable req_c2s_v : std_logic;
  begin
    -- assign variables
    state_v   := state_cs;
    rnw_v     := rnw_m2c_cs;
    req_v     := req_m2c_cs;
    rdy_s2c_v := rdy_s2c_cs;
    ack_c2m_v := ack_c2m_cs;
    oe_v      := oe_cs;
    -- assign data variables
    dati_m2c_v := dati_m2c_cs;
    dato_c2m_v := dato_c2m_cs;
    dati_s2c_v := dati_s2c_cs;
    dato_c2s_v := dato_c2s_cs;
    --
    req_c2s_v := req_c2s_cs;
    --
    case state_v is
      when "000" => -- Z0
        if req_v = '1' and rnw_v = '1' then
          state_v := "001"; -- Transition to 001 (ZL1)
          oe_v := '1';      -- Enable output enable
        elsif req_v = '1' and rnw_v = '0' then
          state_v := "100"; -- Transition to 100 (ZR1)
          dato_c2s_v := dati_m2c_v; -- Daten vom Controller zum Slave leiten (c2s)
          req_c2s_v := '1'; -- Set request for calculation
        end if;
      when "001" => -- ZL1
        if rdy_s2c_v = '1' then
          state_v := "010"; -- Transition to 010 (ZL2)
          ack_c2m_v := '1'; -- Enable ACK for uC
          dato_c2m_v := dati_s2c_v; -- Daten von controller zum master (uC) leiten
        end if;
      when "010" => -- ZL2
        if req_v = '0' then
          state_v := "011"; -- Transition to 011 (ZL3)
          oe_v := '0';
        end if;
      when "011" => -- ZL3
        state_v := "000"; -- Transition to 000 (Z0)
        ack_c2m_v := '0'; -- Disable ACK for uC
      when "100" => -- ZR1
          state_v := "101"; -- Transition to 101 (ZR2)
          ack_c2m_v := '1'; -- Enable ACK for uC
          req_c2s_v := '0'; -- Remove request for calculation
      when "101" => -- ZR2
        if req_v = '0' then
          state_v := "110"; -- Transition to 110 (ZR3)
        end if;
      when "110" => -- ZR3
        state_v := "000"; -- Transition to 000 (Z0)
        ack_c2m_v := '0'; -- Disable ACK for uC
      when others => state_v := "000";
    end case;
    --
    -- assign signals
    ack_c2m_ns  <= ack_c2m_v;
    dato_c2m_ns <= dato_c2m_v;
    dato_c2s_ns <= dato_c2s_v;
    --
    oe_ns <= oe_v;
    req_c2s_ns <= req_c2s_v;
    -- assign state
    state_ns <= state_v;
  end process cobilo;

  -- register
  sequlo:
  process(clk) is
  begin
    if clk = '1' and clk'event then
      if nres = '0' then
        ack_c2m_cs  <= '0';
        dati_m2c_cs <= (others => '0');
        dato_c2m_cs <= (others => '0');
        dati_s2c_cs <= (others => '0');
        dato_c2s_cs <= (others => '0');
        req_m2c_cs  <= '0';
        rnw_m2c_cs  <= '0';
        rdy_s2c_cs  <= '0';
        req_c2s_cs  <= '0';
        state_cs    <= (others => '0');
        oe_cs       <= '0';
      else
        ack_c2m_cs  <= ack_c2m_ns;
        dati_m2c_cs <= dati_m2c;
        dato_c2m_cs <= dato_c2m_ns;
        dati_s2c_cs <= dati_s2c;
        dato_c2s_cs <= dato_c2s_ns;
        req_m2c_cs  <= req_m2c;
        rnw_m2c_cs  <= rnw_m2c;
        rdy_s2c_cs  <= rdy_s2c;
        req_c2s_cs  <= req_c2s_ns;
        state_cs    <= state_ns;
        oe_cs       <= oe_ns;
      end if;
    end if;
  end process sequlo;
  --
  -- Assign to outgoing signals
  ack_c2m <= ack_c2m_cs;
  dato_c2m <= dato_c2m_cs;
  dato_c2s <= dato_c2s_cs;
  --
  req_c2s <= req_c2s_cs;
  oe <= oe_cs;
  -- DEBUG
  state_debug <= state_cs;
end architecture rtl;
