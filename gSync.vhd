

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
    
    
    
    
--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
--XXX
--XXX ENTITY
--XXX

entity gSync is
    generic (
        numberOfSyncFF : positive := 3              -- number of sync FFs (of synchronizer) resp. number of fifo-bits
    );--}generic
    port (
        synco   : out  std_logic;                   -- Out
        synci   : in   std_logic;                   -- In
        clk     : in   std_logic;                   -- CLocK
        sReset  : in   std_logic                    -- Synchronous RESET
    );--}port
    --------------------------------------------------------------------------
    constant msbPos : natural := numberOfSyncFF-1;  -- Most significant bit POsition
end entity gSync;





--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
--XXX
--XXX ARCHITECTURE
--XXX

architecture rtl of gSync is
    
    signal  syncFIFO_cs  : std_logic_vector( msbPos downto 0 )  := (OTHERS=>'0');
    
begin
    
    mixlo:                       -- mixed (combinatorial and sequential) logic
    process ( clk ) is
    begin
        if  '1'=clk and clk'event  then
            if  '0'=sReset  then
                syncFIFO_cs  <=  (OTHERS=>'0');
            else
                syncFIFO_cs  <=  syncFIFO_cs( msbPos-1 downto 0 ) & synci;
            end if;
        end if;
    end process mixlo;
    --
    -- assign result(s) to outgoing port(s)
    synco <= syncFIFO_cs(msbPos);
    
end architecture rtl;