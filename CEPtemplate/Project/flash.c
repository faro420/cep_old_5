/*  
     @author: Mir Farshid Baha
     Contact: farshid.baha@yahoo.com
 */

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_spi.h>
#include <stdint.h>
#include "CE_Lib.h"
#include "tft.h"
#include "flash.h"

// config spi mode
#define SPI_MODE_3          (SPI_CPOL_High | SPI_CPHA_2Edge)    // set spi mode 3 for AT25DF641 ()
#define SPI_CLK_DIV_2       SPI_BaudRatePrescaler_2             // 

//----------------------------------------------------
//================READ==START=========================
//----------------------------------------------------
void read(enum Chip type,uint32_t address, size_t nob){
    uint8_t receiveByte = DUMMY;
    int i;
	uint32_t rc;
    if(address <= MAX_ADDRESS){
		assert(type);
        //sending Op code for READ 
        receiveByte = spi_write_byte(READ_FAST) ;
        //sending the starting address
        receiveByte = send_add(address);   
        //2xDummy transmission to get started
        receiveByte = spi_write_byte(DUMMY); 
        receiveByte = spi_write_byte(DUMMY);
        //The actual reading starts here..
        for(i=0;i < nob;i++){
            SPI3->DR = DUMMY;            
            while(!(SPI3->SR & SPI_SR_RXNE));
            receiveByte = SPI3->DR;
		//TODO
        }//finished reading
        //disconnect
	deassert();
    }
}
//----------------------------------------------------
//================READ==END===========================
//----------------------------------------------------

//----------------------------------------------------
//===============ERASE==START=========================
//----------------------------------------------------
void erase_xKB(enum Chip type, uint32_t address, uint8_t op_code){
	uint8_t receiveByte;
	write_enable(type);
	unprotect_sector(type, address);
	write_enable(type);
	assert(type);
    receiveByte = spi_write_byte(op_code);
    receiveByte = send_add(address) ;
	deassert();
    wait_unitl_rdy(type);
}

void wait_unitl_rdy(enum Chip type)
{
	uint8_t result = 0;
	assert(type);
	spi_write_byte(READ_STATUS_REG);
	do{
		//result = spi_write_byte(DUMMY)<<8;
        result = spi_write_byte(DUMMY);
	}while(result & 0x01);
	deassert();
}

int checkForValidBeginning(uint32_t address)
{
    if((address <= 0x7FF000) && ((address & 0xfff) == 0x0))
    {
        return 1;
    }
    return 0;
}

int checkForValidSize(size_t erase_size)
{
    if ((erase_size <= MAX_SIZE) && !(erase_size % _4KB))
    {
        return erase_size;
    }
    if((erase_size <= MAX_ADDRESS) && ((erase_size & 0xfff) == 0xfff))
    {
        return erase_size+1;
    }
    return 0;
}

int validSuffix(uint32_t address, uint32_t suffix)
{
    if ((address & 0xFFFF) == suffix)
    {
        return 1;
    }
    return 0;
}

void erase_logic(enum Chip type, uint32_t address, size_t erase_size) {
    if(MAX_ADDRESS < erase_size){
        erase_xKB(type, address, ERASE_CHIP);
    }else{
        size_t temp_size;
        uint8_t save_flashy = 0;
        if (checkForValidBeginning(address) && (temp_size = checkForValidSize(erase_size))){
            while (0 < temp_size && save_flashy < MAX_ERASE_OPS ) {
                if (_64KB <= temp_size && validSuffix(address, VALID_64KB_SUFFIX)) {
                    erase_xKB(type, address, ERASE_64KB);
                    address = ((address | _64KB_MASK) + 1) % MAX_SIZE;
                    temp_size -= _64KB;
                }
                else if (_32KB <= temp_size && (validSuffix(address, VALID_32KB_SUFFIX) || validSuffix(address, 0x0)))
                {
                    erase_xKB(type, address, ERASE_32KB);
                    address = ((address | _32KB_MASK) + 1) % MAX_SIZE;
                    temp_size -= _32KB;
                }
                else
                {
                    erase_xKB(type, address, ERASE_4KB);
                    address = ((address | _4KB_MASK) + 1) % MAX_SIZE;
                    temp_size -= _4KB;
                }
                save_flashy++;
            }   
        }
    }
}

//----------------------------------------------------
//===============ERASE==END===========================
//----------------------------------------------------

//----------------------------------------------------
//===========PROGRAM==START===========================
//----------------------------------------------------
void program_interval(enum Chip type, uint32_t address, size_t length, uint8_t* array){
    int i;
    uint8_t receiveByte;
	uint32_t currentIndex = 0;
    erase_logic(type,address,length);
    while(length){
		write_enable(type);
		unprotect_sector(type, address);
		write_enable(type);	
        //sending the programming code and the address to start writing..
		assert(type);
        receiveByte = spi_write_byte(BYTE_PAGE_PROGRAM);
        receiveByte = send_add(address);
        for(i = 0; (i < PAGE_SIZE)&& (length!= 0);i++){
       //TODO
            spi_write_byte(array[currentIndex + i]);
			currentIndex++;
            length--;
        }
        //unplugging
		deassert();	
        //waiting for the procedure to finish
		wait_unitl_rdy(type);
        address = (address | _256B_MASK) + 1;
    }     
}

void program(enum Chip type,uint32_t address, size_t length, uint8_t* array){
    if(address <= MAX_ADDRESS){
            uint32_t difference;
            difference = MAX_ADDRESS - address; //calculate possible space from given address til MAX_ADDRESS
            if(difference  < length){ //check if required space is given
                program_interval(type,address, difference, array); 
                program_interval(type,address, MIN_ADDRESS, array + length - difference);                
            }else{
                program_interval(type, address, length, array);                
            }
    }
}

void assert(enum Chip type){
     if(type == WORK){
         GPIOB->BSRRH = GPIO_Pin_9;
     }else{
         GPIOG->BSRRH = GPIO_Pin_6;
     }
}

void deassert(void){
    GPIOB->BSRRL = GPIO_Pin_9;
    GPIOG->BSRRL = GPIO_Pin_6;
}

uint8_t send_add(uint32_t address){
    uint8_t receiveByte;
    receiveByte = spi_write_byte(address>>16) ; //MSB->LSB,sending 1st address byte      
    receiveByte = spi_write_byte(address>>8);//MSB->LSB,sending 2nd address byte 
    receiveByte = spi_write_byte(address);//MSB->LSB,sending 3rd address byte 
    return receiveByte;
}

uint8_t spi_write_byte(uint8_t data) {
    SPI3->DR = data;                    // write dummy byte into data register
    while(!(SPI3->SR & SPI_SR_RXNE));   // wait until valid data is in rx buffer (RM0090 Chap 28.3.7)
    return SPI3->DR;                    // read data register (RM0090 Chap 28.3.7)
}

void write_enable(enum Chip type){
    assert(type);
    spi_write_byte(WRITE_ENABLE) ;
	deassert();
}

void unprotect_sector(enum Chip type, uint32_t address){
    assert(type);
    spi_write_byte(UNPROTECT_SECTOR) ;
    send_add(address) ;
	deassert();
}

void init_flash(void){
            // clock setup (enable clock for used ports)
    // =============
    //
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;            // enable clock for GPIOC
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;            // enable clock for GPIOG
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI
    
        // SPI
    // =====
    //
    // Set PortB.9 as output (Chip Select 1)
    GPIOB->MODER |= (GPIO_Mode_OUT << (2*9)); 
    // Set PortG.6 as output (Chip Select 2)
    GPIOG->MODER |= (GPIO_Mode_OUT << (2*6));
    
    // Set SPI Pins to alternate port function
    // set IO mode to alternate function (RM0090 Chap 8.3.2)
    GPIOC->MODER |= (GPIO_Mode_AF << (2*10)) | (GPIO_Mode_AF << (2*11)) | (GPIO_Mode_AF << (2*12));
    GPIOC->OSPEEDR |= (GPIO_Fast_Speed << (2*10)) | (GPIO_Fast_Speed << (2*11)) | (GPIO_Fast_Speed << (2*12));
    // set alternate function mode for use with spi (RM0090 Chap 8.3.2)
    GPIOC->AFR[1] |= (GPIO_AF_SPI3 << (4*2)) | (GPIO_AF_SPI3 << (4*3)) | (GPIO_AF_SPI3 << (4*4));
    
    // Set SPI Configuration
    // enable clock for SPI (RM0090 Chap 6.3.13)
    RCC->APB1ENR |= RCC_APB1ENR_SPI3EN;
    // set SPI configuration(RM0090 Chap 28.5.1)    
    SPI3->CR1 = (SPI_CR1_SPE | SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CLK_DIV_2 | SPI_MODE_3);

    // disable chip selects
    deassert();
}
