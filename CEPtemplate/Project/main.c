#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include "CE_Lib.h"
#include <stm32f4xx_spi.h>
#include "flash.h"
#include "a5.h"

#define FPGA
#define WAVE_SIZE 3635944 

// config spi mode
#define SPI_MODE_3          (SPI_CPOL_High | SPI_CPHA_2Edge)    // set spi mode 3 for AT25DF641 ()
#define SPI_CLK_DIV_2       SPI_BaudRatePrescaler_2             // 
/*
 * This macro is used to type values in binary
 */
#define b(n) (                                     \
    (unsigned char)(                               \
          ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
      |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
      |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
      |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                          \
)

/*
 * simplified access to switches S0 - S7
 */
#define  S8   ( !(GPIOH->IDR & (b(1) << 15)) )
#define  S7   ( !(GPIOH->IDR & (b(1) << 12)) )
#define  S6   ( !(GPIOH->IDR & (b(1) << 10)) )
#define  S5   ( !(GPIOF->IDR & (b(1) << 8 )) )
#define  S4   ( !(GPIOF->IDR & (b(1) << 7 )) )
#define  S3   ( !(GPIOF->IDR & (b(1) << 6 )) )
#define  S2   ( !(GPIOC->IDR & (b(1) << 2 )) )
#define  S1   ( !(GPIOI->IDR & (b(1) << 9 )) )

#define  STM32_NVIC_PriorityGroup_0   ((uint32_t)0x7)         /*!< 0 bits for pre-emption priority 4 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_1   ((uint32_t)0x6)         /*!< 1 bits for pre-emption priority 3 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_2   ((uint32_t)0x5)         /*!< 2 bits for pre-emption priority 2 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_3   ((uint32_t)0x4)         /*!< 3 bits for pre-emption priority 1 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_4   ((uint32_t)0x3)         /*!< 4 bits for pre-emption priority 0 bits for subpriority */

/*
 * timer setup
 * ============
 */
#define SYS_FREQ     168000000                             // 168MHz system frequency
#define TIMER_FREQ       44100                             // 44.1Khz requested timer frequency
#define Q28_35 35

// buffer_BUFF_SIZE has to equal 2^n                             (for buffer_INDX_MSK)
#define   buffer_BUFF_SIZE   ( b(1) << 2 )                      /*!< 4 entries buffer depth resp. buffer size */
#define   buffer_EMPTY 0
#define   buffer_INDX_MSK   ( buffer_BUFF_SIZE - 1 )

#define   FACTOR ( ((((168000000ull)<<Q28_35) / ((44100ull)<<Q28_35) - 1ull<<Q28_35)) >>Q28_35 )
typedef struct{
	uint16_t left;
	uint16_t right;	
} Data;

static volatile Data buffer_raw[ buffer_BUFF_SIZE ] = { 0 };     // FIFO1 for raw data
static volatile Data buffer_decoded[ buffer_BUFF_SIZE ] = { 0 }; // FIFO2 for decoded data
static volatile uint16_t bufferElements = 0;                    // bufferElements
static volatile uint16_t bufferRdIndx = 0;                      // buffer Read Index
static volatile int8_t   bufferUnderflow = 0;                   // "buffer UNDERFLOW was detected" state
uint8_t receiveByte = DUMMY;
uint16_t bufferWrIndx = 0;
uint32_t address = 0x0;
uint16_t puffer;

const uint32_t factor = FACTOR;
uint32_t counter  = 0;


//----------------------------------------------------------------------------
//
//  ISR
//
void TIM8_UP_TIM13_IRQHandler(void) {                               // Timer interrupt fired with 44100Hz
	TIM8->SR = ~TIM_SR_UIF;                                         // Reset interrupt flag to receive new interrupts (RM0090 Chap 17.4.5) 
    
    // handle IRQ  resp. do service
    // ==========
    //
    if (bufferElements > buffer_EMPTY)
    {    
        // PWM Output1 Default values for compare register 50% duty cycle
        TIM8->CCR2 = buffer_decoded[bufferRdIndx].left; // (RM0090 Chap 18.3.8)
		// PWM Output 2
        TIM8->CCR3 = buffer_decoded[bufferRdIndx].right;  
		
        bufferRdIndx = (bufferRdIndx + 1) & buffer_INDX_MSK;
		--bufferElements;     
    }
    else
    {
        bufferUnderflow = 1;// error state
    }	

}//TIM8_UP_TIM13_IRQHandler()


//----------------------------------------------------------------------------

void init_timer(void){
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;            // enable clock for GPIOC
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI
	
	resetLED(1); // set trigger pin to low
	
    // set IO mode to alternate function (RM0090 Chap 8.3.2)
	GPIOB->MODER   |= (GPIO_Mode_AF 	<< (GPIO_PinSource0 * 2))	| (GPIO_Mode_AF     << (GPIO_PinSource1 * 2));
	GPIOB->OSPEEDR |= (GPIO_Speed_50MHz << (GPIO_PinSource0 * 2)) 	| (GPIO_Speed_50MHz << (GPIO_PinSource1 * 2));
	GPIOB->OTYPER  |= (GPIO_OType_PP 	<< (GPIO_PinSource0))       | (GPIO_OType_PP    << (GPIO_PinSource1));
	GPIOB->PUPDR   |= (GPIO_PuPd_UP 	<< (GPIO_PinSource0 * 2))   | (GPIO_PuPd_UP     << (GPIO_PinSource1 * 2));
    // set alternate function mode for use as pwm output (RM0090 Chap 8.3.2)
	GPIOB->AFR[0]  |= (GPIO_AF_TIM8 	<< (GPIO_PinSource0 * 4))   | (GPIO_AF_TIM8     << (GPIO_PinSource1 * 4));
        
    
    // Timer
    // =====
    //

    RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;             // enable clock for timer 8         (RM0090 Chap 6.3.14)
    TIM8->CR1 = 0;                                  // disabled timer                   (RM0090 Chap 17.4.1)
    TIM8->CR2 = 0;                                  //                                  (RM0090 Chap 17.4.2)
    TIM8->PSC = 0;                                  // prescaler                        (RM0090 Chap 17.4.11)
    TIM8->ARR = (SYS_FREQ / TIMER_FREQ) -1;         // auto reload register for 44100Hz (RM0090 Chap 17.4.12)
    TIM8->DIER = TIM_DIER_UIE;                      // enable interrupt                 (RM0090 Chap 17.4.4)
    TIM8->CR1 = TIM_CR1_ARPE;                       // enable preload                   (RM0090 Chap 17.4.1)
	TIM8->BDTR = TIM_BDTR_MOE;
    
    // set output compare modes for corresponding IO pins (RM0090 Chap 17.4.7 / 17.4.8) 
	TIM8->CCMR1 = TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;
	TIM8->CCMR2 = TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3PE;
    // set complementary output enable for corresponding IO pins (RM0090 Chap 17.4.9)
	TIM8->CCER = TIM_CCER_CC3NE | TIM_CCER_CC2NE;
    
    NVIC_SetPriorityGrouping(2);                    // set priority
    NVIC_SetPriority(TIM8_UP_TIM13_IRQn, 0);        // set IRQ handler
    NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);             // enable IRQ 
}

uint16_t next_data(){
	SPI3->DR = DUMMY;            
    while(!(SPI3->SR & SPI_SR_RXNE));
    receiveByte = SPI3->DR;
    puffer = receiveByte; 
    SPI3->DR = DUMMY;            
    while(!(SPI3->SR & SPI_SR_RXNE));
    receiveByte = SPI3->DR;
    puffer |= (receiveByte << 8);
#ifdef FPGA
	write_to_FPGA(&puffer);
	read_from_FPGA(&puffer);
#endif
	return puffer;
}

void fill_buffer(void){
	    int i;
        assert(WORK);
        //sending Op code for READ 
        receiveByte = spi_write_byte(READ_FAST) ;
        //sending the starting address
        receiveByte = send_add(address);   
        //2xDummy transmission to get started
        receiveByte = spi_write_byte(DUMMY); 
        receiveByte = spi_write_byte(DUMMY);
        //The actual reading starts here..
        for(i=0;i < 4;i++){        
			puffer = next_data();
            buffer_decoded[bufferWrIndx].left = ((((int16_t)puffer) + 32768)*factor)>>16;
			puffer = next_data();
            buffer_decoded[bufferWrIndx].right = ((((int16_t)puffer) + 32768)*factor)>>16;
			bufferElements = bufferWrIndx+1;
            bufferWrIndx = (bufferWrIndx + 1) & buffer_INDX_MSK;
			counter+=4;
        }//finished reading
}
void refill_buffer(void){
			if(WAVE_SIZE < counter){
				counter = 0;
				deassert();
				assert(WORK);
				 //sending Op code for READ 
				receiveByte = spi_write_byte(READ_FAST) ;
				//sending the starting address
				receiveByte = send_add(address);   
				//2xDummy transmission to get started
				receiveByte = spi_write_byte(DUMMY); 
				receiveByte = spi_write_byte(DUMMY);
				//The actual reading starts here..
			}
            do
            {
				puffer = next_data();
				buffer_decoded[bufferWrIndx].left = ((((int16_t)puffer) + 32768)*factor)>>16;
				puffer = next_data();
				buffer_decoded[bufferWrIndx].right = ((((int16_t)puffer) + 32768)*factor)>>16;
                bufferElements++;
                bufferWrIndx = (bufferWrIndx + 1) & buffer_INDX_MSK;
				counter+=4;
            } while (bufferElements != buffer_BUFF_SIZE);
}


//-------------------------------------FPGA--------------------------------
void fpga_io_init(void) {
  RCC->AHB1ENR |=   RCC_AHB1ENR_GPIOEEN; // enable clock for GPIOE

  //
  GPIOG->OTYPER  = (GPIOG->OTYPER  & ~0xFFF) | 0x000;     // [5:0] as push pull
  GPIOG->OSPEEDR = (GPIOG->OSPEEDR & ~0xFFF) | 0xFFC;     // [5:1] as high speed and [0] as low speed
  GPIOG->PUPDR   = (GPIOG->PUPDR   & ~0xFFF) | 0x000;     // [5:0] have neither pull ups nor pull downs
  GPIOG->MODER   = (GPIOG->MODER   & ~0xFFF) | 0x055;     // [5:4] as IN and [3:0] as OUT
  //
  GPIOG->BSRRH = b(1111);
  // configure (�C<->FPGA) data bus
  GPIOE->OTYPER  = 0x00000000;       // data bus is configured as push pull/GPIO_OType_PP
  GPIOE->OSPEEDR = 0xFFFFFFFF;       // data bus is configured as high speed/GPIO_High_Speed
  GPIOE->PUPDR   = 0x00000000;       // data bus has neither pull ups nor pull downs/GPIO_PuPd_NOPULL
  GPIOE->MODER   = 0x00000000;       // data bus is configured as input (in the beginning)
  // activate FPGA since all signals are configured
  GPIOG->BSRRL = b(1); // (n)swReset <- 1
}


void read_from_FPGA(uint16_t* buffer) {
  GPIOE->MODER = 0x00000000; // data bus is input for �C
  GPIOG->BSRRL = (b(1)<<2);               // signal FPGA that it is read ;  FPGA-RnW <- 1
  GPIOG->BSRRL = (b(1)<<3);               // phase1 :  send out REQuest, marking all signals valid                (hand over bus to FPGA)
  while(( GPIOG->IDR & (b(1)<<5)) == 0 ); // phase2 :  wait for ACKnowledge   resp.   result                      (FPGA is driving bus)
  *buffer = GPIOE->IDR;
  GPIOG->BSRRH = (b(1)<<3);               // phase3 :  handshake for ACK by revoking REQuest, marking results taken and request bus back
  while(( GPIOG->IDR & (b(1)<<5)) != 0 ); // phase4 :  wait for ACKnowledge    resp.                              (bus free again)
}

void write_to_FPGA(uint16_t* data) {
  GPIOG->BSRRH = (b(1)<<2); // rnw
  // This toggle the output mode of the data signal
  GPIOE->MODER = 0x55555555; // enable �C output drivers
  GPIOE->ODR = *data;        // write data to bus
  GPIOG->BSRRL = (b(1)<<3);               // phase1 :  send out REQuest, marking all signals valid                (handover data to FPGA)
  while(( GPIOG->IDR & (b(1)<<5)) == 0 ); // phase2 :  wait for ACKnowledge                                       (FPGA has taken data)
  GPIOG->BSRRH = (b(1)<<3);               // phase3 :  handshake for ACK by revoking REQuest
  while(( GPIOG->IDR & (b(1)<<5)) != 0 ); // phase4 :  wait for ACKnowledge                                       (FPGA is ready for next access)
}

//-------------------------------------FPGA---End--------------------------
//----------------------------------------------------------------------------
//
//  MAIN
//
int main( void ){

    /*
     * general setup
     * =============
     */
    initCEP_Board();                                    // initilize display, leds, buttons, uart and other stuff CE_Lib.c
    init_flash();
    init_timer();
	fpga_io_init(); 
    // GPIOA->MODER = (GPIOA->MODER & ~(b(11) << (GPIO_PinSource4 * 2))) | (GPIO_Mode_AN << (GPIO_PinSource4 * 2));
    // DAC->CR = 0;                                   // ControlRegister: configure / keep defaults     {>RM0090 chap 14.5.1}
    // DAC->CR |= DAC_CR_EN1;                         // ControlRegister: Enable DAC channel1           {>RM0090 chap 14.5.1}
    // DAC->CR |= DAC_CR_WAVE1;
	fill_buffer();
    while(1)
    {
        if (bufferElements != buffer_BUFF_SIZE)
        {
			refill_buffer();
        }
       
        if ( S2 )
        {
            TIM8->CR1 &= ~TIM_CR1_CEN;        
        }
       
        if ( S1 )
        {
            TIM8->CR1 |= TIM_CR1_CEN;            
        }
    }
}//main()
