/*  
     @author: Mir Farshid Baha
     Contact: farshid.baha@yahoo.com
 */
#ifndef __A5
#define __A5
	void fill_buffer(void);
	void refill_buffer(void);
	void fpga_io_init(void) ;
	void read_from_FPGA(uint16_t* buffer);
	void write_to_FPGA(uint16_t* buffer);
#endif//_A5